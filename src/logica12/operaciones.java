package logica12;

public class operaciones {
    public long factorial(long j){
        if (j < 0) {
            throw new IllegalArgumentException("No se puede calcular el factorial de un numero negativo");
        }
        long resultado = 1;
        for (int i = 1; i <= j; i++) {
            if (resultado > Long.MAX_VALUE / i) {
                throw new ArithmeticException("El resultado del factorial excede la capacidad");
            }
            resultado *= i;
        }
        return resultado;
    }

    public double division(double dividendo, double divisor) {
        if (divisor == 0) {
            throw new ArithmeticException("no se puede dividir por cero.");
        }
        return dividendo/divisor;
    }

    private long factorialS(int j) {
        if (j == 0 || j == 1) {
            return 1;
        }
        return j * factorialS(j - 1);
    }

    public long combinatoria(int j, int t) {
        if (j < 0 || t < 0 || t > j) {
            throw new IllegalArgumentException("Los valores de j y t  no deben ser negativos y t no puede ser mayor que j");
        }
        long numerador = factorialS(j);
        long denominador = factorialS(t) * factorialS(j - t);
        return numerador / denominador;
    }
}